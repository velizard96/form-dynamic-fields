$(document).ready(function () {

  let fields = ''
  function generateFields(value) {
    $.ajax({
      url: 'get-fields.php?field=' + value
    })
      .done(function (response) {
        let fields = response
        for (let field of fields) {
          createField(field)
        }
      })
  }

  generateFields($('#type').val())

  $('#type').on("change", function () {
    $('#generated-fields').empty()

    generateFields($(this).val())

    for (let field of fields) {
      createField(field)
    }
  })

  function createField(field) {

    let formGroup = $('<div>')
    formGroup.addClass('form-groups')

    let newField = $('<input>')

    let newLabel = $('<label>')
    newLabel.text(field.label)

    newField.addClass('form-inputs')
    newField.attr('name', field.name)

    if (field.type === 'textarea') {
      newField = $('<textarea>')
      newField.addClass('form-inputs')
    } else {
      newField.attr('type', field.type)
    }

    formGroup.append(newLabel)
    formGroup.append(newField)

    $('#generated-fields').append(formGroup)

  }

  $('#form').on('submit', function (еvent) {
    event.preventDefault()

    let that = $(this)
    url = that.attr('action')
    method = that.attr('method')
    data = {}

    that.find('[name]').each(function (index, value) {
      let that = $(this),
        name = that.attr('name')
      value = that.val()

      data[name] = value
    })

    $.ajax({
      url: url,
      method: method,
      data: data,
      success: function (response) {
        if (response.success == true) {

          $.notify('The form has been Submitted!', "success")

        } else {
          $.notify('Something went wrong', "error")
        }
      }
    })
    return false
  })

})
